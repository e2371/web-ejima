# web-ejima

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```


### Heroku url
https://web-ejima.herokuapp.com/


### Project Description 
Projet test de EJIMA en vue.js

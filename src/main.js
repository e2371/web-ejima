import Vue from 'vue';
import App from './App.vue';

import router from './router';
import store from './store';
import Service from './service/index.ts';
import config from './config';
import axios from 'axios';
import VueAxios from 'vue-axios';
import Dashboard from './components/Dashboard.vue';

import 'bootstrap'; 
import 'bootstrap/dist/css/bootstrap.min.css';

import 'vue-loaders/dist/vue-loaders.css';
import VueLoaders from 'vue-loaders';
import VueAlertify from 'vue-alertify';
 
// Load plugins
// Vue.use(VueLoaders);
Vue.use(VueAlertify, {});

Vue.component('dashboard-layout', Dashboard);
Vue.component('vue-loaders', VueLoaders.component);

Vue.use(VueAxios, axios);

import './main.scss';

Vue.prototype.$service = Service(config, axios);


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

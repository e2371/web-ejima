import Vue from 'vue';
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    userInfos: {}
  },
  mutations: {
    getUserInfos (state) {
      state.userInfos = JSON.parse(localStorage.getItem('userInfos'));
    },
    resetUserInfos (state) {
      state.userInfos = null;
    }
  },
  actions: {
  },
  getters: {
    // adminRole(state) {
    //   return state.adminInfo ? state.adminInfo.role : null;
    // }
  }
})

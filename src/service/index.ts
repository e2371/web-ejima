
function Service (config, axios) {

  const getToken = () => {
    return localStorage.getItem('token');
  };

  const tokenExpire = () => {
    localStorage.removeItem('userInfos');
    localStorage.removeItem('token');
    // this.$router.push({ path: '/' });
  }
 
  const get = (endpoint, params, callback) => {
    axios({
      method: 'get',
      url: config.apiUrl + endpoint,
      params
    }).then(function (response) {
      if (response.data.status == 401) {
        tokenExpire();
      } else {
        if (response.status == 200 && response.data.newToken && response.data.newToken.length > 20) {
          localStorage.setItem('token', response.data.newToken);
        }
        callback(response.data);
      }
    }, function (error) {
      return callback(error.response);
    });
  };

  const post = (endpoint, params, callback) => {
    axios({
      method: 'post',
      url: config.apiUrl + endpoint,
      data: params
    }).then(function (response) {
      if (response.data.status == 401) {
        tokenExpire();
      } else {
        if (response.status == 200 && response.data.newToken && response.data.newToken.length > 20) {
          localStorage.setItem('token', response.data.newToken);
        }
        callback(response.data);
      }
    }, function (error) {
      return callback(error.response);
    });
  };
  
  
  const getLogged = (endpoint, params, callback) => {
    const userToken = getToken();
    axios({
      method: 'get',
      url: config.apiUrl + endpoint,
      headers: { 'Authorization': "bearer " + userToken}, 
      params
    }).then(function (response) {
      if (response.data.status == 401) {
        tokenExpire();
      } else {
        if (response.status == 200 && response.data.newToken && response.data.newToken.length > 20) {
          localStorage.setItem('token', response.data.newToken);
        }
        callback(response.data);
      }
    }, function (error) {
      return callback(error.response);
    });
  };
  
  // const postLogged = (endpoint, params, callback) => {
  //   const userToken = getToken();
  //   axios({
  //     method: 'post',
  //     url: config.apiUrl + endpoint,
  //     headers: { 'Authorization': "bearer " + userToken}, 
  //     data: params
  //   }).then(function (response) {
  //     if (response.data.status == 401) {
  //       tokenExpire();
  //     } else {
  //       if (response.status == 200 && response.data.newToken && response.data.newToken.length > 20) {
  //         localStorage.setItem('token', response.data.newToken);
  //       }
  //       callback(response.data);
  //     }
  //   }, function (error) {
  //     return callback(error.response);
  //   });
  // };

  const putLogged = (endpoint, params, callback) => {
    const userToken = getToken();
    axios({
      method: 'put',
      url: config.apiUrl + endpoint,
      headers: { 'Authorization': "bearer " + userToken}, 
      data: params
    }).then(function (response) {
      if (response.data.status == 401) {
        tokenExpire();
      } else {
        if (response.status == 200 && response.data.newToken && response.data.newToken.length > 20) {
          localStorage.setItem('token', response.data.newToken);
        }
        callback(response.data);
      }
    }, function (error) {
      return callback(error.response);
    });
  };

  const put = (endpoint, params, callback) => {
    axios({
      method: 'put',
      url: config.apiUrl + endpoint,
      // headers: { 'Authorization': "bearer " + userToken}, 
      data: params
    }).then(function (response) {
      if (response.data.status == 401) {
        tokenExpire();
      } else {
        callback(response.data);
      }
    }, function (error) {
      return callback(error.response);
    });
  };
  
  const removeLogged = (endpoint, params, callback) => {
    const userToken = getToken();
    axios({
      method: 'delete',
      url: config.apiUrl + endpoint,
      headers: { 'Authorization': "bearer " + userToken}, 
      data: params
    }).then(function (response) {
      if (response.data.status == 401) {
        tokenExpire();
      } else {
        if (response.status == 200 && response.data.newToken && response.data.newToken.length > 20) {
          localStorage.setItem('token', response.data.newToken);
        }
        callback(response.data);
      }
    }, function (error) {
      return callback(error.response);
    });
  };


  const login = (admin, callback)=> {
    post('users/login',admin,callback);
  }
  
  const register = (user, callback)=> {
    post('users/register', user, callback);
  }
  
  const list = (endpoint, query, callback) => {
    getLogged(endpoint + '/list', query || {}, callback);
  }
  
  const update = (endpoint, dataId, data, callback)=> {
    putLogged(endpoint + '/' + dataId, data, callback);
  }

  const remove = (endpoint, id, callback)=> {
    removeLogged(endpoint + '/remove', {id}, callback);
  }

  return {
    // getMe,
    login,
    register,
    
    get,
    getLogged,
    post,
    put,
    putLogged,

    list,
    update,
    remove

  };
}

export default Service;